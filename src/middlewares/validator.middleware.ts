import { Request,Response,NextFunction } from 'express';
import { NestMiddleware,Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import User from '../entities/user.entity';


@Injectable()
class SignupValidator implements NestMiddleware {

    isEmailValid(email: string): boolean {
        const emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        return Boolean(emailRegex.test(email));
    }

    use(req: Request, res: Response, next: NextFunction){
        
        if(req.body.password.length < 5){
            return res.status(400).json({message: 'Password Length should be greater than equal to 5.'})
        }

        if(!this.isEmailValid(req.body.email)){
            return res.status(400).json({message: 'Not a valid email.'})
        }

        next()

    }

}

@Injectable()
class SigninValidator implements NestMiddleware {

    constructor( @InjectRepository(User) private usersRepository: Repository<User>){}

    isEmailValid(email: string): boolean {
        const emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        return Boolean(emailRegex.test(email));
    }

    async use(req: Request, res: Response, next: NextFunction){
        
        if(!req.body.password || !req.body.email){
            return res.status(400).json({message: 'Both Email & Password are required.'})
        }

        if(!this.isEmailValid(req.body.email)){
            return res.status(400).json({message: 'Not a valid email.'})
        }

        // const findUser = await this.usersRepository.findOneBy({email:req.body.email})

        // if(!findUser){ 
        //     return res.status(400).json({message: `Could not find use with email ${req.body.email}`})
        // }

        next()

    }

}

export {SignupValidator,SigninValidator}