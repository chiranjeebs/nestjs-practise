import { Module, NestModule,MiddlewareConsumer } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SignupValidator,SigninValidator } from '../middlewares/validator.middleware';
import CurdController from '../controllers/user.controller';
import CurdService from '../services/user.services';
import User from '../entities/user.entity';
import Posts from '../entities/posts.entity';
import Config from '../configs/prod.configs'
import PostService from '../services/posts.services';
import PostController from '../controllers/posts.controller';

// This module uses the forFeature() method to define which repositories are registered in the current scope.

@Module({
    imports: [
        TypeOrmModule.forFeature([User,Posts])
    ],
    providers: [CurdService,PostService],
    controllers: [CurdController,PostController]
})

class UserModule implements NestModule {

    configure(consumer:MiddlewareConsumer){

        consumer.apply(SignupValidator).forRoutes('/api/curd/insert/user')
        consumer.apply(SigninValidator).forRoutes('/api/curd/login/user')

    }
}

export default UserModule;