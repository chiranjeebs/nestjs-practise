
export class UserDto {
    email: string;
    phone: string
    firstName:string;
    lastName:string;
    isActive?:boolean;
    password:string;
}

export class UserSigninDto {
    email:string;
    password:string;
}