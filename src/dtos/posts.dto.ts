

export class PostDto {
    headLine:string;
    description:string;
    postUrl?:string;
    authorId?:string|number
}