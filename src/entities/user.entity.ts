import { Entity,Column,PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import PostsModel from './posts.entity';
import Role from '../enums/roles.enums';

@Entity()
class User {

    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    phone:string

    @Column({
        default:false,
    })
    isPhoneVerified:boolean

    @Column()
    email:string;

    @Column({
        type: 'enum',
        enum:Role,
        default:Role.USER
    })
    role:string;

    @Column()
    firstName:string;

    @Column()
    lastName:string;

    @Column({default:true})
    isActive:boolean;

    @Column({nullable:false})
    password:string

    @OneToMany(() => PostsModel, (post) => post.author)
    posts:PostsModel[];

}

export default User;