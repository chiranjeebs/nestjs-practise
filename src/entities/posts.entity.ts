import { Entity,Column,PrimaryGeneratedColumn, OneToMany,ManyToOne } from 'typeorm';
import UserModel from './user.entity'

@Entity()
class Posts {

    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    headLine:string;

    @Column()
    description:string;

    @Column()
    postUrl:string;

    @ManyToOne(()=>UserModel,(user)=>user.posts)
    author:UserModel;

}

export default Posts;