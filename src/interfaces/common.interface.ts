
export interface ApiResult {
    success:boolean,
    message:string,
    token?:string
}

export interface TokenPayload {
    sub:string|number;
    email:string;
    role:string;
}