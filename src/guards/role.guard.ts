import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Request } from 'express';
import {ROLE_KEY} from '../decorators/roles.decorators';
import Role from '../enums/roles.enums';

export class TokenDto {
    id: number;
    role: Role;
}

@Injectable()
class RolesGuard implements CanActivate {

    constructor(private reflector:Reflector) { }

    canActivate(context: ExecutionContext): boolean {

        // 1. Extract required roles from metadata:
        const requiredRoles = this.reflector.getAllAndOverride<Role[]>(ROLE_KEY,[
            context.getHandler(), // Check route/method metadata
            context.getClass(), // Check controller metadata (optional)
        ]);

        // 2. If no required roles defined, allow access:
        if(!requiredRoles){
            return true;
        }

        // 3. Obtain user information from request:
        const { user } = context.switchToHttp().getRequest();
        
        //4. Check if Users role is equal to atlear one of the role
        return requiredRoles.some((role) => {
            return user.role === role
        });
    }

}

export default RolesGuard;