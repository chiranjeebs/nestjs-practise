import { UseGuards, Controller, Get, Req, Param, Body, Post, HttpStatus, Res, Delete, Patch } from '@nestjs/common';
import AuthGuard from '../guards/auth.guard';
import { Request,Response } from 'express';
import PostService from '../services/posts.services';
import {PostDto} from '../dtos/posts.dto';

@Controller('/api/posts/')
class PostController {

    constructor(private readonly postService: PostService){}

    @UseGuards(AuthGuard)
    @Post('/insert')
    NewPost(@Body() postDto:PostDto,@Req() req){
        return this.postService.CreatePost(postDto,req.user.sub)
    }


}


export default PostController;