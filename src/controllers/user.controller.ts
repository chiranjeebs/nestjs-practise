import { UseGuards, UseInterceptors, Controller, Get, Req, Param, Body, Post, HttpStatus, Res, Delete, Patch } from '@nestjs/common';
import Curd from '../services/user.services';
import AuthGuard from '../guards/auth.guard';
import RoleGuard from '../guards/role.guard';
import { Request,Response } from 'express';
import {ApiResult} from '../interfaces/common.interface'
import Role from '../enums/roles.enums';
import {Roles} from '../decorators/roles.decorators';
import {UserDto,UserSigninDto} from '../dtos/user.dto';
import {SignupValidator} from '../middlewares/validator.middleware';

@Controller('/api/curd/')
class CurdController {

    constructor(private readonly curdService: Curd){}

    @Get('/get/users')
    getUsers(){
        return this.curdService.findAll()
    }    

    @Post('/get/user/:id')
    findUser(@Param() params: any){
        console.log(params)
        return this.curdService.findOne(params.id)
    }

    @Delete('/delete/user/:id')
    deleteUser(@Param() params: any){
        return this.curdService.removeOne(params.id)
    }

    @Patch('/update/user/:id')
    updateUser(@Param('id') id: number,@Body() userDto:UserDto){
        return this.curdService.updateOne(id,userDto)
    }

    @Post('/insert/user')
    insertUser(@Body() userDto:UserDto){
        this.curdService.sendOtp(userDto.phone)
        return this.curdService.insertUser(userDto)
    }

    @Post('/login/user')
    async loginUser(@Body() singinDto:UserSigninDto,@Res() res: Response){

        const result = await this.curdService.singinUser(singinDto)

        if(!result.success){
            return res.status(HttpStatus.BAD_REQUEST).json(result)
        }

        return res.status(HttpStatus.OK).json(result)
    }

    @Get('/first')
    firstCurdController(@Req() request: Request): string {
        return this.curdService.firstCurdService();
    }

    @Get('/first/:num')
    secondCurdController(@Req() request: Request, @Param() params: any): string {
        return this.curdService.secondCurdService(params);
    }

    @Post('/first/body/')
    //passthrogh true is used because not to loose compatiability with nest features
    async thirdCurdController(@Body() firstDto: UserDto, @Res({ passthrough: true }) res: Response){

        console.log(firstDto)

        return res.status(HttpStatus.OK).json({message: "Body Received"})

    }

    @UseGuards(AuthGuard)
    @Get('/view/profile')
    ownProfile(@Req() req){
       return req.user
    }

    @UseGuards(AuthGuard,RoleGuard)
    @Roles(Role.ADMIN)
    @Get('/get/profiles')
    getAllProfiles(@Req() req){
        return this.curdService.findAll()
    }

}

export default CurdController