import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtModule,JwtService,JwtModuleOptions } from '@nestjs/jwt';
import Config from './configs/prod.configs';
import UserEntity from './entities/user.entity';
import PostEntity from './entities/posts.entity';
import UserModule from './modules/curd.module';
import RolesGuard from './guards/role.guard';

@Module({
  imports: [

    JwtModule.register({
      global: true,
      secret:Config.JWT_SECRET,
      signOptions:{expiresIn:'200s'}
    }),

    // AccessTokenModule.register({
    //   global: true,
    //   secret:Config.JWT_SECRET,
    //   signOptions:{expiresIn:'60s'}
    // }),

    TypeOrmModule.forRoot({

      type:'mysql',
      host:'localhost',
      port:3306,
      username:'chandan',
      password:'Iamchiranjeeb@98',
      database:'nest_test',
      entities: [UserEntity,PostEntity],
      synchronize:true,
      retryAttempts:20,
      retryDelay:2000

    }),
    UserModule

  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
