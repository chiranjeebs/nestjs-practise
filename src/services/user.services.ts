import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import {UserDto,UserSigninDto} from '../dtos/user.dto';
import {ApiResult} from '../interfaces/common.interface';
import { JwtService } from '@nestjs/jwt';
import { Twilio } from 'twilio';
import {TokenPayload} from '../interfaces/common.interface';
import Config from '../configs/prod.configs';
import * as bcrypt from 'bcrypt';
import User from '../entities/user.entity';

@Injectable()
class Curd {

    private twilioClient: Twilio;

    constructor(
        @InjectRepository(User) private usersRepository: Repository<User>,
        private jwtService: JwtService,
    ){
        this.twilioClient = new Twilio(Config.TWILLIO_ACCOUNT_SID, Config.TWILLIO_AUTH_TOKEN);
    }

    firstCurdService(): string {
        return 'First Curd Function...'
    }

    secondCurdService(param:any): string {
        return `Second Curd Function With ${param.num}`
    }

    findAll():Promise<User[]> {
        return this.usersRepository.find()
    }

    findOne(id:number):Promise<User | null> {
        return this.usersRepository.findOneBy({id})
    }

    async insertUser(userDto:UserDto){
        userDto.password = await bcrypt.hash(userDto.password,10)
        const user =  this.usersRepository.create(userDto)
        return this.usersRepository.save(user)
    }

    async sendOtp(phoneNumber: string){

        // this.twilioClient.verify.v2.services('VA4be18d043e0a8023fd5af1e94504f13c').verifications.create({ to: phoneNumber, channel: 'sms' })
        // .then(msg => {
        //     console.log('Mssggggggggggg')
        //     console.log(msg)
        // })
        // .catch(err => {
        //     console.log("Error in Twillio Verifications")
        //     console.log(err)
        // })

        const msg = await this.twilioClient.messages.create({
            body:"oooooooo 111111111111111",
            from:Config.TWILLIO_PHONE_NUMBER,
            to:phoneNumber
        })

        console.log("Send Otpppppppppppppp")
        console.log(msg)

    }

    async updateOne(id:number,attrs:Partial<User>){
        const user = await this.usersRepository.findOneBy({id})

        if(!user){
            throw new Error(`User not found with id ${id}`)
        }

        Object.assign(user,attrs)

        const updateUser = await this.usersRepository.save(user)

        return updateUser
    }

    async removeOne(id:number){
        const removedUser = await this.usersRepository.delete(id)
        return removedUser
    }

    async singinUser(singupDto:UserSigninDto):Promise<ApiResult>{
        const {email, password} = singupDto
        const findUser = await this.usersRepository.findOneBy({email})
        if(!findUser) {
            return {
                success:false,
                message:"User Not Found"
            }
        }
        const isMatch = await bcrypt.compare(password, findUser.password);
        if(!isMatch) {
            return {
                success:false,
                message:"Invalid Credentials"
            }
        }
        const payload:TokenPayload = {
            sub:findUser.id,
            email:findUser.email,
            role:findUser.role
        }

        console.log('Token SErvices')

        const token = await this.jwtService.signAsync(payload)
        return {
            success:true,
            message:"User",
            token
        }
    }

}

export default Curd