import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import User from '../entities/user.entity';
import Posts from '../entities/posts.entity';
import {PostDto} from '../dtos/posts.dto';


@Injectable()
class PostService{

    constructor(@InjectRepository(User) private usersRepository: Repository<User>,@InjectRepository(Posts) private postRepository: Repository<Posts>){}

    async CreatePost(postDto:PostDto,authorId:number){

        const user = await this.usersRepository.findOneBy({id:authorId})

        delete user.password

        const postNew = {
            ...postDto,
            author:user
        }
        
        const newPost = this.postRepository.create(postNew)

        return this.postRepository.save(newPost)
    }

}

export default PostService;